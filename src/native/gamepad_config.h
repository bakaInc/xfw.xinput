// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright (c) 2018-2022 Mikhail Paulyshka

#pragma once

namespace XFW::XInput {
    class GamepadConfiguration
    {
    public:
        //LX
        [[nodiscard]] bool IsXAxisInverted() const {
            return _axis_x_inverted;
        }

        [[nodiscard]] float GetLeftThreshold() const {
            return _left_threshold;
        }

        [[nodiscard]] float GetRightThreshold() const {
            return _right_threshold;
        }

        //LY
        [[nodiscard]] bool IsYAxisInverted() const {
            return _axis_y_inverted;
        }

        [[nodiscard]] float GetForward100Threshold() const {
            return _forward_100_threshold;
        }
        [[nodiscard]] float GetForward50Threshold() const {
            return _forward_50_threshold;
        }
        [[nodiscard]] float GetForward25Threshold() const {
            return _forward_25_threshold;
        }

        [[nodiscard]] float GetBackward100Threshold() const {
            return _backward_100_threshold;
        }
        [[nodiscard]] float GetBackward50Threshold() const {
            return _backward_50_threshold;
        }

        //RX
        [[nodiscard]] float GetCameraLeftThreshold() const {
            return _camera_left_threshold;
        }
        [[nodiscard]] float GetCameraRightThreshold() const {
            return _camera_right_threshold;
        }

        //RY
        [[nodiscard]] float GetCameraDownThreshold() const {
            return _camera_down_threshold;
        }
        [[nodiscard]] float GetCameraUpThreshold() const {
            return _camera_up_threshold;
        }


        //Triggers
        [[nodiscard]] float GetLeftTriggerThreshold() const {
            return _trigger_left_threshold;
        }
        [[nodiscard]] float GetRightTriggerThreshold() const {
            return _trigger_right_threshold;
        }


    private:
        float _axis_x_inverted = false;
        float _axis_y_inverted = false;

        float _forward_100_threshold = 0.5f;
        float _forward_50_threshold = 0.3f;
        float _forward_25_threshold = 0.175f;

        float _backward_100_threshold = -0.5f;
        float _backward_50_threshold = -0.175f;

        float _left_threshold = -0.4f;
        float _right_threshold = 0.4f;

        float _camera_left_threshold = -0.2f;
        float _camera_right_threshold = 0.2f;

        float _camera_down_threshold = -0.2f;
        float _camera_up_threshold = 0.2f;

        float _trigger_left_threshold = -0.4f;
        float _trigger_right_threshold = 0.4f;
    };
}