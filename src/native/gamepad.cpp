// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright (c) 2018-2022 Mikhail Paulyshka


//XFW.XInput
#include "constants.h"
#include "gamepad.h"

namespace XFW::XInput {
	Gamepad::Gamepad()
	{
	}

	Gamepad::~Gamepad(){
		Stop();
	}

	bool Gamepad::Start()
	{
		if (_thread.joinable()) {
			return false;
		}

		_thread_termination.clear();
		_thread = std::thread(&Gamepad::threadFunction, this);
		return true;
	}

	bool Gamepad::Stop()
	{
		if (!_thread.joinable()) {
			return false;
		}

		_thread_termination.test_and_set();
		_thread.join();
		return true;
	}

	void Gamepad::threadFunction()
	{
		while (!_thread_termination.test())
		{
			for (int uid = 0; uid < DirectX::GamePad::MAX_PLAYER_COUNT; uid++)
			{
				auto state = _gamepad.GetState(uid);
				if (!state.IsConnected())
				{
					continue;
				}

				processGamepadState(state);
				break;
			}

			std::this_thread::sleep_for(_polling_interval);
		}
	}

	void Gamepad::processGamepadState(const DirectX::GamePad::State& state)
	{
		BigworldKey moveCommand = { BigworldKey::BW_NONE };
		CruiseControlMode cruiseMode = CruiseControlMode::NONE;

		const auto ly = state.thumbSticks.leftY;
		if (ly > _config.GetForward100Threshold())
		{
			moveCommand |= BigworldKey::BW_FORWARD;
			cruiseMode = CruiseControlMode::FWD100;
		}
		else if (ly > _config.GetForward50Threshold())
		{
			moveCommand |= BigworldKey::BW_FORWARD | BigworldKey::BW_CRUISE_CONTROL50;
			cruiseMode = CruiseControlMode::FWD50;
		}
		else if (ly > _config.GetForward25Threshold())
		{
			moveCommand |= BigworldKey::BW_FORWARD | BigworldKey::BW_CRUISE_CONTROL25;
			cruiseMode = CruiseControlMode::FWD25;
		}
		else if (ly < _config.GetBackward100Threshold())
		{
			moveCommand |= BigworldKey::BW_BACKWARD;
			cruiseMode = CruiseControlMode::BCKW100;
		}
		else if (ly < _config.GetBackward50Threshold())
		{
			moveCommand |= BigworldKey::BW_BACKWARD | BigworldKey::BW_CRUISE_CONTROL50;
			cruiseMode = CruiseControlMode::BCKW50;
		}

		const auto lx = state.thumbSticks.leftX;
		if (lx > _config.GetRightThreshold())
		{
			moveCommand |= BigworldKey::BW_ROTATE_RIGHT;
		}
		else if (lx < _config.GetLeftThreshold())
		{
			moveCommand |= BigworldKey::BW_ROTATE_LEFT;
		}

		//Triggers
		int dz = 0;
		if (state.triggers.left > _config.GetLeftTriggerThreshold())
		{
			dz = -10 * state.triggers.left;
		}
		if (state.triggers.right > _config.GetRightTriggerThreshold())
		{
			dz = 10 * state.triggers.right;
		}

		//RX
		float dx = 0.0f;
		auto rx = state.thumbSticks.rightX;
		if (rx > _config.GetCameraRightThreshold())
		{
			dx = 5.0f * rx;
		}
		else if (rx < _config.GetCameraLeftThreshold())
		{
			dx = 5.0f * rx;
		}

		//RY
		float dy = 0.0f;
		const auto ry = state.thumbSticks.rightY;
		if (ry > _config.GetCameraUpThreshold())
		{
			dy = -5.0f * ry;
		}
		else if (ry < _config.GetCameraDownThreshold())
		{
			dy = -5.0f * ry;
		}

		_avatar.ProcessInput(moveCommand, cruiseMode, dx, dy, dz);
	}
}