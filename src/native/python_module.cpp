// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright (c) 2018-2022 Mikhail Paulyshka


// pybind11
#include <pybind11/pybind11.h>

// XFW.XInput
#include "gamepad.h"


PYBIND11_MODULE(XFW_XInput, m) {
    m.doc() = "XFW XInput module";

    pybind11::class_<XFW::XInput::Gamepad>(m, "Gamepad")
        .def(pybind11::init<>())
        .def("start", &XFW::XInput::Gamepad::Start, "start gamepad polling thread")
        .def("stop", &XFW::XInput::Gamepad::Stop, "stop gamepad polling thread");
}
