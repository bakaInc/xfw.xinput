// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright (c) 2018-2022 Mikhail Paulyshka

// stdlib
#include <atomic>
#include <chrono>
#include <memory>
#include <thread>

// DirectXTK
#include <GamePad.h>

// XFW.Xinput
#include "gamepad_config.h"
#include "wot_avatar.h"

using namespace std::literals::chrono_literals;

namespace XFW::XInput {
    class Gamepad {
    public:
        Gamepad();
        ~Gamepad();
        
        bool Start();
        bool Stop();

    private:
        void threadFunction();
        void processGamepadState(const DirectX::GamePad::State& state);

    private:
        DirectX::GamePad _gamepad;
        GamepadConfiguration _config;
        WoTAvatar _avatar;

        std::atomic_flag _thread_termination{};
        std::thread _thread{};

    private:
        static constexpr auto _polling_interval = 1ms;
    };
}

