"""
This file is part of the XVM Framework project.

Copyright (c) 2018-2022 XVM Team.

XVM Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XVM Framework is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

#
# Imports
#

# cpython
import logging

# xfw.native
from xfw_native.python import XFWNativeModuleWrapper



#
# Globals
#

__xinput_native = None
__xinput_native_gamepad = None
__xinput_initialized = False



#
# XFW Loader
#

def xfw_is_module_loaded():
    global __xinput_initialized
    return __xinput_initialized

def xfw_module_init():
    global __xinput_native
    global __xinput_native_gamepad
    global __xinput_initialized

    __xinput_native = XFWNativeModuleWrapper('com.modxvm.xfw.xinput', 'xfw_xinput.pyd', 'XFW_XInput')
    __xinput_native_gamepad = __xinput_native.Gamepad()

    if __xinput_native_gamepad is not None:
        __xinput_native_gamepad.start()
        __xinput_initialized = True
